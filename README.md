# project anlegen
oc new-project <project> oder oc project <project>

# build config erstellen
oc new-build https://gitlab.com/cbrugger/openshift-swagger-editor.git

# aus gebauten image app erstellen
oc new-app openshift-swagger-editor

# route anlegen
oc create route edge --insecure-policy=Redirect --service=openshift-swagger-editor